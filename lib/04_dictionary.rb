class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(pair)
    if pair.class == String
      key = pair
      value = nil
    else
      key = pair.keys.first
      value = pair.values.first
    end
    @entries[key] = value
  end

  def keywords
    @entries.keys
  end

  def include?(word)
    @entries.has_key?(word)
  end

  def find(word)
    @entries.select do |k,v|
      k.start_with?(word)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def printable
    string = []
    keywords.each do |key|
    string << ("[#{key}]" + " \"#{@entries[key]}\"")
    end
    string.join("\n")
  end
end
