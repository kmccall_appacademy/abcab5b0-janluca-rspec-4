class Book
  attr_accessor :title

  def title=(book_title)
    words = book_title.split
    words[0] = words[0].capitalize
    lowercase_words = %w(in the of and to a an)
    words.map! do |word|
      !lowercase_words.include?(word) ? word.capitalize : word
    end
    @title = words.join(" ")
  end

end
