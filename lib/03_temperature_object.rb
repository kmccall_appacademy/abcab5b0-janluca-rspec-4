class Temperature
  attr_accessor :f, :c

  def initialize(options = {})
    @f = options[:f]
    @c = options[:c]
  end

  def in_celsius
    @c == nil ? ((@f - 32) / 1.8).round(1) : @c
  end

  def in_fahrenheit
    @f == nil ? (@c * 1.8 + 32).round(1) : @f
  end

  def self.from_celsius(temp)
    self.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    self.new(:f => temp)
  end

end

class Celsius < Temperature
  def initialize(temp)
    @c = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @f = temp
  end
end
