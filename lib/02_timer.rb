class Timer
  attr_accessor :seconds, :minutes, :hours

  def initialize
    @hours = 0
    @minutes = 0
    @seconds = 0
  end

  def seconds=(secs)
    @seconds = secs
  end

  def convert_seconds
    seconds = @seconds % 60
    seconds < 10 ? "0#{seconds}" : seconds
  end

  def convert_minutes
    minutes = (@seconds / 60) % 60
    minutes < 10 ? "0#{minutes}" : minutes
  end

  def convert_hours
    hours = (@seconds / 3600) % 60
    hours < 10 ? "0#{hours}" : hours
  end

  def time_string
    "#{convert_hours}:#{convert_minutes}:#{convert_seconds}"
  end
end
